class LocalizationBuilder {
  private var currentLanguage: String? = null
  private var fallbackLanguage: String? = null
  private var whitelistKey = "whitelist"
  private var languages = mutableMapOf<Language, MutableMap<String, List<DocumentNode>>>()

  fun currentLanguage (language: Language): LocalizationBuilder {
    this.currentLanguage = language
    return this
  }

  fun fallbackLanguage (language: Language): LocalizationBuilder {
    this.fallbackLanguage = language
    return this
  }

  fun whitelistKey (whitelistKey: String): LocalizationBuilder {
    this.whitelistKey = whitelistKey
    return this
  }

  fun add (language: Language, source: Map<String, String>): LocalizationBuilder {
    this.languages[language] = source.asIterable().fold(mutableMapOf<String, List<DocumentNode>>()) { map, (key, value) ->
      map[key] = Document.parse(value)
      map
    }
    return this
  }

  fun finalize (): Localization {
    val currentLanguage = this.currentLanguage ?: throw AssertionError.Fail()
    val fallbackLanguage = this.fallbackLanguage ?: throw AssertionError.Fail()

    return Localization(currentLanguage, fallbackLanguage, this.whitelistKey, this.languages)
  }
}
