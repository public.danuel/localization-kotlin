import utils.isAlphabet
import utils.isNumeric
import java.lang.Exception

class Document (private val stream: ParserStream) {
  companion object {
    private fun parse(stream: ParserStream): List<DocumentNode> =
      Document(stream).parse()

    fun parse(source: String): List<DocumentNode> =
      try {
        this.parse(ParserStream(source))
      } catch (_: Exception) {
        listOf(DocumentNode.Text(source))
      }
  }

  private fun parse (): List<DocumentNode> {
    val nodeList = mutableListOf<DocumentNode>()
    while (true) {
      val linebreakCount = this.stream.consumeLinebreak()
      if (linebreakCount != 0) {
        nodeList += DocumentNode.Linebreak(linebreakCount)
        continue
      }

      if (this.stream.isEof) {
        break
      }

      nodeList += this.parseChildren()
    }

    return nodeList
  }

  private fun parseChildren (): List<DocumentNode> {
    val nodeList = mutableListOf<DocumentNode>()
    while (true) {
      if (this.stream.starts(with = "</")) {
        break
      }

      nodeList += this.parseChild() ?: break
    }

    return nodeList
  }

  private fun parseChild (): DocumentNode? {
    if (this.stream.isEol || this.stream.isEof) {
      return null
    }

    val whitespaceCount = this.stream.consumeWhitespace()
    return if (whitespaceCount != 0) {
      DocumentNode.Whitespace(whitespaceCount)
    } else {
      this.parseCurly() ?: this.parseTag() ?: this.parseText()
    }
  }

  private fun parseCurly (): DocumentNode? {
    if (!this.stream.starts(with = "{")) {
      return null
    }

    this.stream.consumeChar()
    this.stream.consumeWhitespace()
    val isVariable = this.stream.starts(with = "$")
    if (isVariable) {
      this.stream.consumeChar()
    }

    val name = this.parseName()
    this.stream.consumeWhitespace()
    Assertion.equals(source = stream.consumeChar(), expected = '}');
    return if (isVariable) {
      DocumentNode.Variable(name)
    } else {
      DocumentNode.Reference(name)
    }
  }

  private fun parseTag (): DocumentNode? {
    if (!this.stream.starts(with = "<")) {
      return null
    }

    this.stream.consumeChar()
    val name = this.parseName()
    val attributes = this.parseAttributes()
    if (this.stream.starts(with = "/>")) {
      this.stream.consume(count = 2)
      return DocumentNode.InlineTag(name, attributes)
    }

    Assertion.equals(source = this.stream.consumeChar(), expected = '>')
    val nodeList = this.parseChildren()
    Assertion.equals(source = this.stream.consumeChar(), expected = '<')
    Assertion.equals(source = this.stream.consumeChar(), expected = '/')
    Assertion.truthy(isTruthy = this.stream.starts(with = name))
    this.stream.consume(count = name.length)
    Assertion.equals(source = this.stream.consumeChar(), expected = '>')
    return DocumentNode.BlockTag(name, attributes, nodeList)
  }

  private fun parseName (): String {
    var isHyphened = false
    val name = this.stream.consume { char, index ->
      if (index == 0) {
        return@consume char.isAlphabet || Assertion.unreachable()
      }

      if (char.toString() == "-") {
        Assertion.falsy(isHyphened)
        isHyphened = true
        return@consume true
      }

      isHyphened = false
      return@consume char.isAlphabet || char.isNumeric || char == '-'
    }
    Assertion.truthy(name)
    Assertion.truthy(name.last() != '-')
    Assertion.falsy(name.length == 1)
    return name
  }

  private fun parseAttributes (): Map<String, ParserStreamValue> {
    if (this.stream.isEol || this.stream.starts(with = "/") || !this.stream.starts(with = ">")) {
      return mapOf()
    }

    val attributes = mutableMapOf<String, ParserStreamValue>()

    this.stream.consumeChar()
    while (true) {
      this.stream.consumeWhitespace()
      if (this.stream.starts(with = "/") || this.stream.starts(with = ">")) {
        break
      }

      this.stream.consumeWhitespace()
      val (name, value) = this.parseAttribute() ?: break
      attributes[name] = value
    }

    return attributes
  }

  private fun parseAttribute (): Pair<String, ParserStreamValue>? {
    if (this.stream.starts(with = "/") || this.stream.starts(with = ">")) {
      return null
    }

    val name = this.parseName()
    if (!this.stream.starts(with = "=")) {
      return Pair(name, ParserStreamValue.Null)
    }

    this.stream.consumeChar()
    this.parseAttributeValue()?.let {
      return Pair(name, it)
    }

    return Assertion.unreachable()
  }

  private fun parseAttributeValue (): ParserStreamValue? =
    this.stream.consumeValue()

  private fun parseText (): DocumentNode =
    DocumentNode.Text(this.stream.consume { char, _ -> when (char) {
      '{', '<', '\n' -> false
      else -> true
    } })
}
