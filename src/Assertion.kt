internal const val DEFAULT_MESSAGE = "Unexpected Conditions"

object Assertion {
  fun truthy (isTruthy: Boolean, message: String = DEFAULT_MESSAGE) {
    if (!isTruthy) {
      throw AssertionError.Truthy(message)
    }
  }

  fun truthy (source: String, message: String = DEFAULT_MESSAGE) {
    if (source.isEmpty()) {
      throw AssertionError.Truthy(message)
    }
  }

  fun falsy (isTruthy: Boolean, message: String = DEFAULT_MESSAGE) {
    if (isTruthy) {
      throw AssertionError.Falsy(message)
    }
  }

  fun falsy (source: String, message: String = DEFAULT_MESSAGE) {
    if (source.isNotEmpty()) {
      throw AssertionError.Falsy(message)
    }
  }

  fun <T> equals(source: T, expected: T, message: String = DEFAULT_MESSAGE) {
    if (source != expected) {
      throw AssertionError.Equals(message)
    }
  }

  fun <T> notEquals (source: T, expected: T, message: String = DEFAULT_MESSAGE) {
    if (source == expected) {
      throw AssertionError.NotEquals(message)
    }
  }

  fun fail (message: String = DEFAULT_MESSAGE) {
    throw AssertionError.Fail(message)
  }

  fun <T> unreachable (message: String = DEFAULT_MESSAGE): T {
    throw AssertionError.Unreachable(message)
  }
}
