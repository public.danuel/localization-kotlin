internal typealias Attributes = Map<String, ParserStreamValue>

sealed class DocumentNode {
  data class BlockTag(val name: String, val attributes: Attributes, val children: List<DocumentNode>): DocumentNode()
  data class InlineTag(val name: String, val attributes: Attributes): DocumentNode()
  data class Reference(val name: String): DocumentNode()
  data class Variable(val name: String): DocumentNode()
  data class Text(val value: String): DocumentNode()
  data class Linebreak(val count: Int): DocumentNode()
  data class Whitespace(val count: Int): DocumentNode()
}
