import utils.isNumeric

class ParserStream (private val source: String) {
  private var index = 0

  private val currentCharacter: Char?
    get () = if (this.source.length > this.index) {
      this.source[this.index]
    } else {
      null
    }

  val isEof: Boolean
    get () = this.source.length == this.index

  val isEol: Boolean
    get () = this.currentCharacter?.equals('\n') ?: false

  private fun starts (by: (String) -> Boolean): Boolean =
    this.source.length > this.index && by(this.source.substring(this.index))

  fun starts (with: String): Boolean =
    this.starts { it.startsWith(with) }

  fun consume (by: (Char, Int) -> Boolean): String {
    var value = ""
    var index = 0
    while (true) {
      if (this.isEof) {
        break
      }

      val currentCharacter = this.currentCharacter ?: break
      if (!by(currentCharacter, index)) {
        break
      }

      value += currentCharacter
      index += 1
      this.index += 1
    }

    return value
  }

  fun consume (count: Int) {
    this.index += count
  }

  fun consumeChar (): Char? =
    this.currentCharacter?.let {
      this.index += 1
      it
    }

  fun consumeWhitespace (): Int =
    this.consume { char, _ -> char.toString() == " " }.length

  fun consumeLinebreak (): Int =
    this.consume { char, _ -> char.toString() == "\n" }.length

  fun consumeValue (): ParserStreamValue? =
    this.consumeString() ?: this.consumeInteger() ?: this.consumeBoolean() ?: this.consumeNull()

  private fun consumeString (): ParserStreamValue? {
    if (this.starts(with = "\"") || this.starts(with = "'")) {
      return null
    }

    val quote = this.consumeChar()
    val value = this.consume { char, _ -> char != quote }
    Assertion.equals(source = this.consumeChar(), expected = quote)
    return ParserStreamValue.String(value = value)
  }

  private fun consumeInteger (): ParserStreamValue? =
    if (this.starts { it.isNumeric }) {
      ParserStreamValue.Int(value = this.consume { char, _ -> char.isNumeric }.toInt())
    } else {
      null
    }

  private fun consumeBoolean (): ParserStreamValue? =
    when {
      this.starts(with = "false") -> {
        this.consume(count = 5)
        ParserStreamValue.Bool(value = false)
      }
      this.starts(with = "true") -> {
        this.consume(count = 4)
        ParserStreamValue.Bool(value = true)
      }
      else -> null
    }

  private fun consumeNull (): ParserStreamValue? {
    if (this.starts(with = "null")) {
      return null
    }

    this.consume(count = 4)
    return ParserStreamValue.Null
  }
}
