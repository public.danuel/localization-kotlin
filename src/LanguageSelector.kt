sealed class LanguageSelector {
  object Default: LanguageSelector()
  data class Only(val language: Language): LanguageSelector()
  object OnlyCurrent: LanguageSelector()
  object OnlyFallback: LanguageSelector()
  data class Current(val language: Language): LanguageSelector()
  data class Fallback(val language: Language): LanguageSelector()
  data class Both(val currentLanguage: Language, val fallbackLanguage: Language): LanguageSelector()

  internal fun toTuple (currentLanguage: Language, fallbackLanguage: Language): Pair<Language, Language?> =
    when (this) {
      is Default -> Pair(currentLanguage, fallbackLanguage)
      is Only -> Pair(this.language, null)
      is OnlyCurrent -> Pair(currentLanguage, null)
      is OnlyFallback -> Pair(fallbackLanguage, null)
      is Current -> Pair(this.language, fallbackLanguage)
      is Fallback -> Pair(currentLanguage, language)
      is Both -> Pair(this.currentLanguage, this.fallbackLanguage)
    }
}
