fun main () {
  test1()
  test2()
  test3()
  test4()
}

fun test1 () {
  val localization = LocalizationBuilder()
    .currentLanguage("en")
    .fallbackLanguage("en")
    .add("en", mapOf())
    .finalize()
  println(localization.get("foo"))
  println(localization.getAll("foo"))
  println(localization.format<String>("foo", mapOf()))
  println(localization.formatArray<String>("foo", mapOf()))
  println(localization.get("foo-bar"))
  println(localization.getAll("foo-bar"))
  println(localization.format<String>("foo-bar", mapOf()))
  println(localization.formatArray<String>("foo-bar", mapOf()))
}

fun test2 () {
  val localization = LocalizationBuilder()
    .currentLanguage("en")
    .fallbackLanguage("en")
    .add("en", mapOf(
      "foo" to "hello world",
      "foo-0" to "hello world",
      "foo-bar" to "hello world",
      "foo-bar-0" to "hello world"
    ))
    .finalize()
  println(localization.get("foo"))
  println(localization.getAll("foo"))
  println(localization.format<String>("foo", mapOf()))
  println(localization.formatArray<String>("foo", mapOf()))
  println(localization.get("foo-bar"))
  println(localization.getAll("foo-bar"))
  println(localization.format<String>("foo-bar", mapOf()))
  println(localization.formatArray<String>("foo-bar", mapOf()))
}

fun test3 () {
  val localization = LocalizationBuilder()
    .currentLanguage("en")
    .fallbackLanguage("en")
    .add("en", mapOf(
      "foo" to "hello {bar}",
      "bar" to "world",
      "foo-0" to "hello {bar}",
      "foo-bar" to "hello {bar}",
      "foo-bar-0" to "hello {bar}"
    ))
    .finalize()
  println(localization.get("foo"))
  println(localization.getAll("foo"))
  println(localization.format<String>("foo", mapOf()))
  println(localization.formatArray<String>("foo", mapOf()))
  println(localization.get("foo-bar"))
  println(localization.getAll("foo-bar"))
  println(localization.format<String>("foo-bar", mapOf()))
  println(localization.formatArray<String>("foo-bar", mapOf()))
}

fun test4 () {
  val localization = LocalizationBuilder()
    .currentLanguage("en")
    .fallbackLanguage("en")
    .add("en", mapOf(
      "foo" to "hello {\$bar}",
      "foo-0" to "hello {\$bar}",
      "foo-bar" to "hello {\$bar}",
      "foo-bar-0" to "hello {\$bar}"
    ))
    .finalize()
  println(localization.get("foo"))
  println(localization.getAll("foo"))
  println(localization.format("foo", mapOf("bar" to "world")))
  println(localization.formatArray("foo", mapOf("bar" to "world")))
  println(localization.format("foo", mapOf("bar" to 123)))
  println(localization.formatArray("foo", mapOf("bar" to 123)))
}
