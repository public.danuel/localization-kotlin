interface StringConvertible {
  override fun toString (): String
}
