interface MapConvertible <T: StringConvertible> {
  fun toMap (): Map<String, T>
}
