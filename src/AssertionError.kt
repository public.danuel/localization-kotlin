sealed class AssertionError: Throwable() {
  data class Truthy(override val message: String = DEFAULT_MESSAGE): AssertionError()
  data class Falsy(override val message: String = DEFAULT_MESSAGE): AssertionError()
  data class Equals(override val message: String = DEFAULT_MESSAGE): AssertionError()
  data class NotEquals(override val message: String = DEFAULT_MESSAGE): AssertionError()
  data class Fail(override val message: String = DEFAULT_MESSAGE): AssertionError()
  data class Unreachable(override val message: String = DEFAULT_MESSAGE): AssertionError()
}
