sealed class ParserStreamValue {
  data class String(val value: kotlin.String): ParserStreamValue()
  data class Int(val value: kotlin.Int): ParserStreamValue()
  data class Bool(val value: Boolean): ParserStreamValue()
  object Null: ParserStreamValue()
}
