internal typealias Language = String
internal typealias Args<T> = Map<String, T>
internal typealias LocaleDictionary = Map<String, List<DocumentNode>>

class Localization (
  private val currentLanguage: Language,
  private val fallbackLanguage: Language,
  private val whitelistKey: String,
  private val languages: Map<Language, LocaleDictionary>
) {
  private fun locales (language: Language): Map<String, List<DocumentNode>>? =
    this.languages[language]

  private fun locale (key: String, language: Language): Pair<Language, List<DocumentNode>>? {
    val nodeList = this.locales(language)?.get(key) ?: return null
    return Pair(language, nodeList)
  }

  private fun locale (key: String, currentLanguage: Language, fallbackLanguage: Language): Pair<Language, List<DocumentNode>>? =
    this.locale(key, currentLanguage) ?: this.locale(key, fallbackLanguage)

  private fun locale (key: String, language: LanguageSelector): Pair<Language, List<DocumentNode>>? {
    val (currentLanguage, fallbackLanguage) = language.toTuple(this.currentLanguage, this.fallbackLanguage)
    val fallback = fallbackLanguage ?: return this.locale(key, currentLanguage)
    return this.locale(key, currentLanguage, fallback)
  }

  private fun <T> format (language: Language, node: DocumentNode, args: Args<T>, referenceList: List<String>): String =
    when (node) {
      is DocumentNode.BlockTag ->
        this.formatArray(language, node.children, args, referenceList)
      is DocumentNode.InlineTag ->
        this.formatArray(language, listOf(), args, referenceList)
      is DocumentNode.Reference -> {
        if (!referenceList.contains(node.name)) {
          this.locales(language)?.get(node.name)?.let {
            this.formatArray(language, it, args, referenceList + node.name)
          } ?: ""
        } else {
          ""
        }
      }
      is DocumentNode.Variable ->
        args[node.name].toString() ?: ""
      is DocumentNode.Text ->
        node.value
      is DocumentNode.Linebreak ->
        "\n".repeat(node.count)
      is DocumentNode.Whitespace ->
        " '".repeat(node.count)
    }

  private fun <T> formatArray (language: Language, nodeList: List<DocumentNode>, args: Args<T>, referenceList: List<String>): String =
    nodeList.joinToString("") { this.format(language, it, args, referenceList) }

  fun get (key: String, language: LanguageSelector = LanguageSelector.Default): String =
    this.format(key, mapOf<String, String>(), language)

  fun getAll (key: String, language: LanguageSelector = LanguageSelector.Default): List<String> =
    this.formatArray(key, mapOf<String, String>(), language)

  fun getAll (key: String, whitelistKey: String, language: LanguageSelector = LanguageSelector.Default): List<String> =
    this.formatArray(key, mapOf<String, String>(), whitelistKey, language)

  fun <T> format (key: String, args: Args<T>, language: LanguageSelector = LanguageSelector.Default): String {
    val (currentLanguage, nodeList) = this.locale(key, language) ?: return ""
    return this.formatArray(currentLanguage, nodeList, args, listOf())
  }

  fun <T> formatArray (key: String, args: Args<T>, language: LanguageSelector = LanguageSelector.Default): List<String> {
    val (currentLanguage, fallbackLanguage) = language.toTuple(this.currentLanguage, this.fallbackLanguage)

    val messageList = mutableListOf<String>()
    for (index in 0..Int.MAX_VALUE) {
      val (_, nodeList) = this.locale("$key-$index", currentLanguage) ?: break
      val message = this.formatArray(currentLanguage, nodeList, args, listOf())
      messageList += message
    }
    if (messageList.isNotEmpty()) {
      return messageList
    }

    val fallback = fallbackLanguage ?: return messageList
    for (index in 0..Int.MAX_VALUE) {
      val (_, nodeList) = this.locale("$key-$index", fallback) ?: break
      val message = this.formatArray(fallback, nodeList, args, listOf())
      messageList += message
    }
    return messageList
  }

  fun <T> formatArray (key: String, args: Args<T>, whitelistKey: String, language: LanguageSelector = LanguageSelector.Default): List<String> {
    val (currentLanguage, fallbackLanguage) = language.toTuple(this.currentLanguage, this.fallbackLanguage)
    val messageKey = "$key-$whitelistKey"

    val currentWhitelistMessage = this.get(messageKey, LanguageSelector.Only(currentLanguage))
    if (currentWhitelistMessage.isEmpty()) {
      val fallback = fallbackLanguage ?: return listOf()
      val fallbackWhitelistMessage = this.get(messageKey, LanguageSelector.Only(fallback))
      return fallbackWhitelistMessage
        .split(",")
        .map { this.format("$key-$it", args) }
        .filter { it.isNotEmpty() }
    }

    return currentWhitelistMessage
      .split(",")
      .map { this.format("$key-$it", args) }
      .filter { it.isNotEmpty() }
  }
}
