package utils

val Char.isNumeric: Boolean
  get () = ('0'..'9').contains(this)

val Char.isAlphabet: Boolean
  get () = ('a'..'z').contains(this) || ('A'..'Z').contains(this)
