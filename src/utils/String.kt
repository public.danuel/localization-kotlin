package utils

val String.isNumeric: Boolean
  get () = ("0".."9").contains(this)
